aspect BackendDirectedAPI {

  public void RelationComponent.generateDirectedZeroOneAPI(StringBuilder sb) {
    String setMethodDecl = getTypeUse().decl() + " " + getTypeUse().decl() + ".set" + nameCapitalized() + "(" + ofTypeDecl() + " o)";

    // Set
    sb.append(ind(1) + "public " + setMethodDecl + " {\n");
    if (!isOpt()) {
      sb.append(ind(2) + "assertNotNull(o);\n");
    }
    sb.append(ind(2) + "set" + getImplAttributeName() + "(o);\n");
    sb.append(ind(2) + "return this;\n");
    sb.append(ind(1) + "}\n");
  }

  public void RelationComponent.generateDirectedManyAPI(StringBuilder sb) {

    // Get
    if (useJastAddNames) {
      String getMethodDecl1 = "java.util.List<" + ofTypeDecl() + "> " + getTypeUse().decl() + ".get" + nameCapitalized() + "s()";
      String getMethodDecl2 = "java.util.List<" + ofTypeDecl() + "> " + getTypeUse().decl() + ".get" + nameCapitalized() + "List()";
      // getXs
      sb.append(ind(1) + "public " + getMethodDecl1 + " {\n");
      sb.append(ind(2) + "return get" + nameCapitalized() + "List();\n");
      sb.append(ind(1) + "}\n");

      // getXList
      sb.append(ind(1) + "public " + getMethodDecl2 + " {\n");
    } else {
      String getMethodDecl = "java.util.List<" + ofTypeDecl() + "> " + getTypeUse().decl() + "." + name() + "()";
      sb.append("public " + getMethodDecl + " {\n");
    }
    sb.append(ind(2) + ASTNode.listInterface + "<" + ofTypeDecl() + "> l = get" + getImplAttributeName() + "();\n");
    // resolve the entire list
    if (resolverHelper | serializer) {
      sb.append(ind(2) + "if (l != null) {\n");
      sb.append(ind(3) + "boolean changed = false;\n");
      sb.append(ind(3) + "for (int i = 0; i < l.size(); i++) {\n");
      sb.append(ind(4) + ofTypeDecl() + " element = l.get(i);\n");
      sb.append(ind(4) + "if (element." + isUnresolvedMethod + "()) {\n");
      sb.append(ind(5) + "changed = true;\n");
      sb.append(ind(5) + ofTypeDecl() + " resolvedElement = " + resolvePrefix + "" + nameCapitalized() + "" + resolvePostfix + "(element." + asUnresolvedMethod + "().get" + unresolvedTokenMethod + "(), i);\n");
      sb.append(ind(5) + "l.set(i, resolvedElement);\n");
      sb.append(ind(4) + "}\n");
      sb.append(ind(3) + "}\n");
      sb.append(ind(3) + "if (changed) {\n");
      sb.append(ind(4) + "set" + getImplAttributeName() + "(l);\n");
      sb.append(ind(3) + "}\n");
      sb.append(ind(2) + "}\n");
    }
    sb.append(ind(2) + "return l != null ? java.util.Collections.unmodifiableList(l) : java.util.Collections.emptyList();\n");
    sb.append(ind(1) + "}\n");

    // Add
    String addMethodDecl1 = "void " + getTypeUse().decl() + ".add" + (useJastAddNames ? "" : "To") + nameCapitalized() + "(" + ofTypeDecl() + " o)";
    sb.append(ind(1) + "public " + addMethodDecl1 + " {\n");
    sb.append(ind(2) + "assertNotNull(o);\n");
    sb.append(ind(2) + ASTNode.listInterface + "<" + ofTypeDecl() + "> list = " + getImplAttributeField() + ";\n");
    sb.append(ind(2) + "if (list == null) {\n");
    sb.append(ind(3) + "list = new " + ASTNode.listClass + "<>();\n");
    sb.append(ind(2) + "}\n");
    sb.append(ind(2) + "list.add(o);\n");
    sb.append(ind(2) + "set" + getImplAttributeName() + "(list);\n");
    sb.append(ind(1) + "}\n");

    // Insert / add at specific position
    String addMethodDecl2 = "void " + getTypeUse().decl() + ".add" + (useJastAddNames ? "" : "To") + nameCapitalized() + "(int index, " + ofTypeDecl() + " o)";
    sb.append(ind(1) + "public " + addMethodDecl2 + " {\n");
    sb.append(ind(2) + "assertNotNull(o);\n");
    sb.append(ind(2) + ASTNode.listInterface + "<" + ofTypeDecl() + "> list = " + getImplAttributeField() + ";\n");
    sb.append(ind(2) + "if (list == null) {\n");
    sb.append(ind(3) + "list = new " + ASTNode.listClass + "<>();\n");
    sb.append(ind(2) + "}\n");
    sb.append(ind(2) + "list.add(index, o);\n");
    sb.append(ind(2) + "set" + getImplAttributeName() + "(list);\n");
    sb.append(ind(1) + "}\n");

    // Remove
    String removeMethodDecl = "void " + getTypeUse().decl() + ".remove" + (useJastAddNames ? "" : "From") + nameCapitalized() + "(" + ofTypeDecl() + " o)";
    sb.append(ind(1) + "public " + removeMethodDecl + " {\n");
    sb.append(ind(2) + "assertNotNull(o);\n");
    sb.append(ind(2) + ASTNode.listInterface + "<" + ofTypeDecl() + "> list = " + getImplAttributeField() + ";\n");
    sb.append(ind(2) + "if (list != null && list.remove(o)) {\n");
    sb.append(ind(3) + "set" + getImplAttributeName() + "(list);\n");
    sb.append(ind(2) + "}\n");
    sb.append(ind(1) + "}\n");
  }
}
