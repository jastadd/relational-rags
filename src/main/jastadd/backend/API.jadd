aspect BackendAPI {
  public void Relation.generateAPI(StringBuilder sb) {
    sb.append(ind(1) + "// api for " + prettyPrint() + "\n");
    if (getLeft().isNavigable()) {
      sb.append(ind(1) + "// left direction for " + prettyPrint() + "\n");
      getLeft().generateAPI(sb);
    }
    if (getRight().isNavigable()) {
      sb.append(ind(1) + "// right direction for " + prettyPrint() + "\n");
      getRight().generateAPI(sb);
    }
    sb.append("\n");
  }

  public void RelationComponent.generateAPI(StringBuilder sb) {
    if (opposite().isNavigable()) {
      if (multiplicityOne() || multiplicityOpt()) {
        generateGetOne(sb);
        if (opposite().multiplicityOne() || opposite().multiplicityOpt()) {
          generateBiOneOne(sb);
        } else if (opposite().multiplicityMany()) {
          generateBiOneMany(sb);
        }
        if (isOpt()) {
          generateExtraOptAPI(sb);
        }
      } else if (multiplicityMany()) {
        if (opposite().multiplicityOne() || opposite().multiplicityOpt()) {
          generateBiManyOne(sb);
        } else if (opposite().multiplicityMany()) {
          generateBiManyMany(sb);
        }
      }
    } else {
      if (multiplicityOne() || multiplicityOpt()) {
        generateGetOne(sb);
        generateDirectedZeroOneAPI(sb);
        if (isOpt()) {
          generateExtraOptAPI(sb);
        }
      } else if (multiplicityMany()) {
        generateDirectedManyAPI(sb);
      }
    }
  }

  public void RelationComponent.generateGetOne(StringBuilder sb) {
    sb.append(ind(1) + "public " + ofTypeDecl() + " " + getTypeUse().decl() + ".");
    if (useJastAddNames) {
      sb.append("get" + nameCapitalized());
    } else {
      sb.append(name());
    }
    sb.append("() {\n");
    if (resolverHelper | serializer) {
      sb.append(ind(2) + "if (" + getImplAttributeField() + " != null && " + getImplAttributeField() + "." + isUnresolvedMethod + "()) {\n");
        sb.append(ind(3) + "if (" + getImplAttributeField() + "." + asUnresolvedMethod + "().get" + unresolvedPrefix + "ResolveOpposite()) {\n");
          sb.append(ind(4) + "set" + nameCapitalized() + "(resolve" + nameCapitalized() + resolvePostfix + "(" + getImplAttributeField() + "." + asUnresolvedMethod + "().get" + unresolvedPrefix + "Token()));\n");
        sb.append(ind(3) + "} else {\n");
          sb.append(ind(4) + "set" + getImplAttributeName() + "(resolve" + nameCapitalized() + resolvePostfix + "(" + getImplAttributeField() + "." + asUnresolvedMethod + "().get" + unresolvedPrefix + "Token()));\n");
        sb.append(ind(3) + "}\n");
      sb.append(ind(2) + "}\n");
    }
    sb.append(ind(2) + "return get" + getImplAttributeName() + "();\n");
    sb.append(ind(1) + "}\n");
  }

  public void RelationComponent.generateExtraOptAPI(StringBuilder sb) {
    // has
    sb.append(ind(1) + "public boolean " + getTypeUse().decl());
    sb.append(".has" + nameCapitalized() + "() {\n");
    sb.append(ind(2) + "return ");
    if (useJastAddNames) {
      sb.append("get" + nameCapitalized());
    } else {
      sb.append(name());
    }
    sb.append("() != null;\n");
    sb.append(ind(1) + "}\n");

    // clear
    sb.append(ind(1) + "public void " + getTypeUse().decl());
    sb.append(".clear" + nameCapitalized() + "() {\n");
    sb.append(ind(2) + "set" + nameCapitalized() + "(null);\n");
    sb.append(ind(1) + "}\n");
  }

  inh Relation Direction.relation();
  inh Relation RelationComponent.relation();
  eq Relation.getChild().relation() = this;
  eq Program.getChild().relation() = null;

  inh boolean RelationComponent.isNavigable();
  eq Relation.getLeft().isNavigable() = getDirection().isNavigableLeftToRight();
  eq Relation.getRight().isNavigable() = getDirection().isNavigableRightToLeft();
  eq Program.getChild().isNavigable() = false;

  syn boolean Direction.isNavigableRightToLeft() = true;
  eq RightDirection.isNavigableRightToLeft() = false;
  syn boolean Direction.isNavigableLeftToRight() = true;
  eq LeftDirection.isNavigableLeftToRight() = false;

  public String RelationComponent.nameCapitalized() {
    return name().substring(0,1).toUpperCase() + name().substring(1);
  }
}
