package org.jastadd.relast.scanner;

import org.jastadd.relast.parser.RelAstParser.Terminals;

%%

%public
%final
%class RelAstScanner
%extends beaver.Scanner

%type beaver.Symbol
%function nextToken
%yylexthrow beaver.Scanner.Exception
%scanerror RelAstScanner.ScannerError

%line
%column
%{
  private StringBuilder stringLitSb = new StringBuilder();

  private beaver.Symbol sym(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }

  private beaver.Symbol sym(short id, String text) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), text);
  }


  public static class ScannerError extends Error {
    public ScannerError(String message) {
      super(message);
    }
  }
%}

WhiteSpace = [ ] | \t | \f | \n | \r | \r\n
TraditionalComment   = [/][*][^*]*[*]+([^*/][^*]*[*]+)*[/]
EndOfLineComment = "//" [^\n\r]*
Comment = {TraditionalComment} | {EndOfLineComment}

ID = [a-zA-Z$_][a-zA-Z0-9$_]*

%%
{WhiteSpace} { /* ignore */ }
{Comment}    { /* ignore */ }

"abstract"   { return sym(Terminals.ABSTRACT); }
"rel"        { return sym(Terminals.RELATION); }

";"          { return sym(Terminals.SCOL); }
":"          { return sym(Terminals.COL); }
"::="        { return sym(Terminals.ASSIGN); }
"*"          { return sym(Terminals.STAR); }
"."          { return sym(Terminals.DOT); }
","          { return sym(Terminals.COMMA); }
"<"          { return sym(Terminals.LT); }
">"          { return sym(Terminals.GT); }
"["          { return sym(Terminals.LBRACKET); }
"]"          { return sym(Terminals.RBRACKET); }
"/"          { return sym(Terminals.SLASH); }
"?"          { return sym(Terminals.QUESTION_MARK); }
"->"         { return sym(Terminals.RIGHT); }
"<-"         { return sym(Terminals.LEFT); }
"<->"        { return sym(Terminals.BIDIRECTIONAL); }

// ID
{ID}         { return sym(Terminals.ID); }
<<EOF>>      { return sym(Terminals.EOF); }

[^]            { throw new ScannerError((yyline+1) +"," + (yycolumn+1) + ": Illegal character <"+yytext()+">"); }
