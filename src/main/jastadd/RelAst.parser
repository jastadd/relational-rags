Program goal =
  /* empty */              {: return new Program(); :}
  | type_decl.t goal.p     {: p.getTypeDeclList().insertChild(t, 0); return p; :}
  | relation.r goal.p      {: p.getRelationList().insertChild(r, 0); return p; :}
  ;

TypeDecl type_decl =
  ID type_decl_super.s components_opt.c SCOL
    {:
      TypeDecl result = new TypeDecl();
      result.setID(ID);
      result.setAbstract(false);
      result.setSuperOpt(s);
      result.setComponentList(c);
      return result;
    :}
  | ABSTRACT ID type_decl_super.s components_opt.c SCOL
      {:
        TypeDecl result = new TypeDecl();
        result.setID(ID);
        result.setAbstract(true);
        result.setSuperOpt(s);
        result.setComponentList(c);
        return result;
      :}
  ;

Opt type_decl_super =
  /* empty */ {: return new Opt(); :}
  | COL s_type_use.u {: return new Opt(u); :}
  ;

SimpleTypeUse s_type_use =
  ID {: return new SimpleTypeUse(ID); :}
  ;

ArrayList inner_type_use
    = ID
    | inner_type_use DOT ID
    ;

TypeUse type_use =
  parameterized_type_use.p    {: return p; :}
  | inner_type_use.p            {: return new SimpleTypeUse((String)p.stream().map( x -> ((Symbol)x).value.toString()).collect(java.util.stream.Collectors.joining("."))); :}
  ;
ParameterizedTypeUse parameterized_type_use =
  inner_type_use.i LT type_use_list.l GT      {: return new ParameterizedTypeUse((String)i.stream().map( x -> ((Symbol)x).value.toString()).collect(java.util.stream.Collectors.joining(".")), l); :}
  ;
List type_use_list =
  type_use.u                              {: return new List().add(u); :}
  | type_use_list.l COMMA type_use.u      {: return l.add(u); :}
  ;

List components_opt =
  /* empty */               {: return new List(); :}
  | ASSIGN components.l     {: return l; :}
  ;

List components =
  /* empty */                   {: return new List(); :}
  | components.l component.c    {: return l.add(c); :}
  ;

Component component =
    ID COL s_type_use.u                       {: return new NormalComponent(ID, u); :}
  | s_type_use.u                              {: return new NormalComponent(u.getID(), u); :}
  // List
  | ID COL s_type_use.u STAR                  {: return new ListComponent(ID, u); :}
  | s_type_use.u STAR                         {: return new ListComponent(u.getID(), u); :}
  // Opt
  | LBRACKET ID COL s_type_use.u RBRACKET     {: return new OptComponent(ID, u); :}
  | LBRACKET s_type_use.u RBRACKET            {: return new OptComponent(u.getID(), u); :}
  // NTA list
  | SLASH ID COL s_type_use.u STAR SLASH      {: return new NTAListComponent(ID, u); :}
  | SLASH s_type_use.u STAR SLASH             {: return new NTAListComponent(u.getID(), u); :}
  // NTA opt
  | SLASH LBRACKET ID COL s_type_use.u RBRACKET SLASH {: return new NTAOptComponent(ID, u); :}
  | SLASH LBRACKET s_type_use.u RBRACKET SLASH {: return new NTAOptComponent(u.getID(), u); :}
  // NTA
  | SLASH ID COL s_type_use.u SLASH           {: return new NTAComponent(ID, u); :}
  | SLASH s_type_use.u SLASH                  {: return new NTAComponent(u.getID(), u); :}
  // NTA Token
  | SLASH LT ID COL type_use.u GT SLASH     {: return new NTATokenComponent(ID, u); :}
  | SLASH LT ID GT SLASH                      {: return new NTATokenComponent(ID, new SimpleTypeUse("String")); :}
  // Token
  | LT ID COL type_use.u GT                   {: return new TokenComponent(ID, u); :}
  | LT ID GT                                  {: return new TokenComponent(ID, new SimpleTypeUse("String")); :}
  ;

Relation relation =
  RELATION relation_comp.l direction relation_comp.r SCOL
   {:
      Relation result = new Relation();
      if (direction instanceof LeftDirection) {
        result.setLeft(r);
        result.setDirection(new RightDirection());
        result.setRight(l);
      } else {
        result.setLeft(l);
        result.setDirection(direction);
        result.setRight(r);
      }
      return result;
   :}
  ;

RelationComponent relation_comp =
  // One
  s_type_use.u DOT ID                       {: return new OneRelationComponent(ID, u); :}
  | s_type_use.u                            {: return new OneRelationComponent("", u); :}
  // Optional
  | s_type_use.u DOT ID QUESTION_MARK       {: return new OptionalRelationComponent(ID, u); :}
  | s_type_use.u QUESTION_MARK              {: return new OptionalRelationComponent("", u); :}
  // Many
  | s_type_use.u DOT ID STAR                {: return new ManyRelationComponent(ID, u); :}
  | s_type_use.u STAR                       {: return new ManyRelationComponent("", u); :}
  ;

Direction direction =
  RIGHT {: return new RightDirection(); :}
  | LEFT {: return new LeftDirection(); :}
  | BIDIRECTIONAL {: return new Bidirectional(); :}
  ;
