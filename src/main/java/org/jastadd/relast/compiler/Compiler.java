package org.jastadd.relast.compiler;

import beaver.Parser;
import org.jastadd.relast.ast.*;
import org.jastadd.relast.compiler.options.*;
import org.jastadd.relast.compiler.options.CommandLine.CommandLineException;
import org.jastadd.relast.parser.RelAstParser;
import org.jastadd.relast.scanner.RelAstScanner;

import java.io.*;
import java.util.*;
import java.util.List;

public class Compiler {

  private ArrayList<Option<?>> options;
  private FlagOption optionWriteToFile;
  private FlagOption optionPrintAST;
  private StringOption optionListClass;
  private StringOption optionJastAddList;
  private StringOption optionGrammarName;
  private EnumOption optionSerializer;
  private FlagOption optionResolverHelper;
  private FlagOption optionUseJastaddNames;
  private FlagOption optionQuiet;
  private FlagOption optionVersion;
  private CommandLine commandLine;

  public Compiler(String[] args) throws CommandLineException {
    options = new ArrayList<>();
    addOptions();

    commandLine = new CommandLine(options);
    commandLine.parse(args);

    if (optionVersion.isSet()) {
      System.out.println(readVersion());
      return;
    }

    printMessage("Running RelAST " + readVersion());

    if (commandLine.getArguments().size() < 1) {
      error("specify at least one input file");
    }

    List<String> filenames = commandLine.getArguments();
    Program p = parseProgram(filenames);

    if (optionJastAddList.isSet()) {
      printMessage("JastAdd List type is set to " + optionJastAddList.getValue());
      ASTNode.jastAddListType = optionJastAddList.getValue();
    }


    if (optionResolverHelper.isSet() || optionSerializer.isSet()) {
      // get a list of all (abstract or not) non-terminals
      List<TypeDecl> nonTerminals = new ArrayList<>();
      for (TypeDecl typeDecl : p.getTypeDecls()) {
        nonTerminals.add(typeDecl);
      }
    }

    if (!p.errors().isEmpty()) {
      if (optionPrintAST.isSet()) {
        System.out.println(p.dumpTree());
      }
      System.err.println("Errors:");
      for (ErrorMessage e : p.errors()) {
        System.err.println(e);
      }
      System.exit(1);
    } else {

      if (optionListClass.isSet()) {
        printMessage("ListClass is set to " + optionListClass.getValue());
        ASTNode.listClass = optionListClass.getValue();
      }

      if (optionUseJastaddNames.isSet()) {
        ASTNode.useJastAddNames = true;
      }

      String grammarName = "Grammar";
      if (optionGrammarName.isSet()) {
        grammarName = optionGrammarName.getValue();
      }

      if (optionWriteToFile.isSet()) {

        if (optionSerializer.isSet()) {
          ASTNode.serializer = true;
          switch (optionSerializer.getValue()) {
            case "jackson":
              writeToFile(grammarName + "Serializer.jadd", p.generateJacksonSerializer());
              break;
            case "jackson-json-pointer":
              ASTNode.jsonPointer = true;
              writeToFile(grammarName + "Serializer.jadd", p.generateJacksonSerializer());
              break;
            case "jackson-manual-references":
              ASTNode.manualReferences = true;
              writeToFile(grammarName + "Serializer.jadd", p.generateJacksonSerializer());
              break;

          }
        }

        if (optionResolverHelper.isSet()) {
          ASTNode.resolverHelper = true;
        }

        if (optionResolverHelper.isSet() || optionSerializer.isSet()) {
          writeToFile(grammarName + "ResolverStubs.jrag", p.generateResolverStubs());
        }

        if (optionSerializer.isSet() || optionResolverHelper.isSet()) {
          writeToFile(grammarName + "RefResolver.jadd", p.generateRewriteToSuperTypeStub());
        }

        writeToFile(grammarName + ".ast", p.generateAbstractGrammar());
        writeToFile(grammarName + ".jadd", p.generateAspect());
      } else if (optionPrintAST.isSet()) {
        System.out.println(p.dumpTree());
      } else {
        printMessage(p.generateAbstractGrammar());
        printMessage(p.generateAspect());
      }
    }
  }

  /**
   * Reads the version string.
   *
   * The version string is read from the property file
   * src/main/resources/Version.properties. This
   * file should be generated during the build process. If it is missing
   * then there is some problem in the build script.
   *
   * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
   * @return the read version string, or <code>version ?</code>
   */
  private String readVersion() {
    try {
      ResourceBundle resources = ResourceBundle.getBundle("RelASTVersion");
      return resources.getString("version");
    } catch (MissingResourceException e) {
      return "version ?";
    }
  }

  public static void main(String[] args) {
    try {
      new Compiler(args);
    } catch (CommandLineException e) {
      System.err.println(e.getMessage());
      System.exit(1);
    }
  }

  private void printMessage(String message) {
    if (!optionQuiet.isSet()) {
      System.out.println(message);
    }
  }

  private void writeToFile(String filename, String str) {
    try {
      PrintWriter writer = new PrintWriter(filename);
      writer.print(str);
      writer.close();
    } catch (Exception e) {
      e.printStackTrace();
      System.exit(1);
    }
  }

  private void addOptions() {
    optionWriteToFile = addOption(new FlagOption("file", "write output to files <filename>Gen.ast and <filename>Gen.jadd"));
    optionPrintAST = addOption(new FlagOption("ast", "print AST (ignores quiet option)"));
    optionListClass = addOption(new StringOption("listClass", "determine the class name of the nonterminal reference list"));
    optionGrammarName = addOption(new StringOption("grammarName", "name of the generated grammar and aspect (without file extension)"));
    optionResolverHelper = addOption(new FlagOption("resolverHelper", "create a subtype for each type containing a string that can be used to resolve the type later"));
    optionJastAddList = addOption(new StringOption("jastAddList", "set the name of the List type in JastAdd (has to match the option '--List' or its default List)"));
    optionUseJastaddNames = addOption(new FlagOption("useJastAddNames", "generate names in the form of addX, removeX and setX. If omitted, the default, original naming scheme resulting in addToX, removeFromX and setX will be used."));
    optionSerializer = addOption(new EnumOption("serializer", "generate a (de-)serializer", Arrays.asList("jackson", "jackson-json-pointer", "jackson-manual-references"), "jackson"));
    optionQuiet = addOption(new FlagOption("quiet", "do not output anything on stdout"));
    optionVersion = addOption(new FlagOption("version", "print version and exit"));
  }

  private <OptionType extends Option<?>> OptionType addOption(OptionType option) {
    options.add(option);
    return option;
  }

  private Program parseProgram(List<String> fileNames) {

    Program program = new Program();

    for (String fileName : fileNames) {
      FileReader reader = null;
      try {
        reader = new FileReader(fileName);
      } catch (FileNotFoundException e) {
        System.err.println(e.getMessage());
        System.exit(1);
      }

      parse(program, reader, fileName);
    }
    return program;
  }

  private void parse(Program program, Reader reader, String file) {
    RelAstScanner scanner = new RelAstScanner(reader);
    RelAstParser parser = new RelAstParser();

    try {
      Program newProgram = (Program) parser.parse(scanner);
      for (TypeDecl typeDecl : newProgram.getTypeDeclList()) {
        typeDecl.setFileName(file);
        program.addTypeDecl(typeDecl);
      }
      for (Relation relation : newProgram.getRelationList()) {
        relation.setFileName(file);
        program.addRelation(relation);
      }
    } catch (IOException e) {
      error(e.getMessage());
    } catch (Parser.Exception e) {
      System.err.println("Parse error in file " + file);
      System.err.println(e.getMessage());
      System.exit(1);
    }
  }

  protected void error(String message) {
    System.err.println("Error: " + message);
    System.err.println();
    System.err.println("Usage: java -jar relast.jar [--option1] [--option2=value] ...  <filename1> <filename2> ... ");
    System.err.println("Options:");
    System.err.print(commandLine.printOptionHelp());
    System.exit(1);
  }
}

