aspect JsonPointer {

  refine RefCreatorStubs eq ASTNode.createReference() = jsonPointer();

  syn String ASTNode.jsonPointer() {
    if (getParent() == null) {
      return "";
    } else {
      return jsonPointerInh();
    }
  }
  inh String ASTNode.jsonPointerInh();
  eq Root.A(int index).jsonPointerInh() {
    return this.jsonPointer() + "/children/A/" + index;
  }
  eq Root.B(int index).jsonPointerInh() {
    return this.jsonPointer() + "/children/B/" + index;
  }
  eq Root.C().jsonPointerInh() {
    return this.jsonPointer() + "/children/C";
  }
  eq C.D1().jsonPointerInh() {
    return this.jsonPointer() + "/children/D1";
  }
  eq C.D2().jsonPointerInh() {
    return this.jsonPointer() + "/children/D2";
  }
  eq C.D3(int index).jsonPointerInh() {
    return this.jsonPointer() + "/children/D3/" + index;
  }

  // context-independent name resolution
  refine RefResolverStubs eq ASTNode.globallyResolveRootByToken(String id) {
    return (Root) resolveJsonPointer(id);
  }

  // context-independent name resolution
  refine RefResolverStubs eq ASTNode.globallyResolveAByToken(String id) {
    return (A) resolveJsonPointer(id);
  }

  // context-independent name resolution
  refine RefResolverStubs eq ASTNode.globallyResolveBByToken(String id) {
    return (B) resolveJsonPointer(id);
  }

  // context-independent name resolution
  refine RefResolverStubs eq ASTNode.globallyResolveCByToken(String id) {
    return (C) resolveJsonPointer(id);
  }

  // context-independent name resolution
  refine RefResolverStubs eq ASTNode.globallyResolveDByToken(String id) {
    return (D) resolveJsonPointer(id);
  }

  // context-independent name resolution
  refine RefResolverStubs eq ASTNode.globallyResolveNamedElementByToken(String id) {
    return (NamedElement) resolveJsonPointer(id);
  }

  syn ASTNode ASTNode.resolveJsonPointer(String pointer) = root().resolveJsonPointer(pointer.split("/"), 1);
  ASTNode ASTNode.root() {
    if (getParent() == null) return this;
    else return getParent().root();
  }
  syn ASTNode ASTNode.resolveJsonPointer(String[] pointer, int index) {
    if (index < pointer.length) {
      throw new RuntimeException("found wrong child  " + pointer[index + 1] + " in class " + this.getClass().getSimpleName());
    } else {
      throw new RuntimeException("Cannot resolve JSON pointer for class " + this.getClass().getSimpleName());
    }
  }
  eq Root.resolveJsonPointer(String[] pointer, int index) {
    if (pointer.length == 0 || pointer.length == index) {
      return this;
    } else if (pointer.length == index + 1) {
      throw new RuntimeException("there is only one child called " + pointer[index]);
    } else {
      switch (pointer[index + 1]) {
        case "A":
          return getA(Integer.valueOf(pointer[index + 2])).resolveJsonPointer(pointer, index + 3);
        case "B":
          return getB(Integer.valueOf(pointer[index + 2])).resolveJsonPointer(pointer, index + 3);
        case "C":
          return getC().resolveJsonPointer(pointer, index + 2);
        default:
          return super.resolveJsonPointer(pointer, index);
      }
    }
  }
  eq A.resolveJsonPointer(String[] pointer, int index) {
    if (pointer.length == 0 || pointer.length == index) {
      return this;
    } else if (pointer.length == index + 1) {
      throw new RuntimeException("there is only one child called " + pointer[index]);
    } else {
      switch (pointer[index + 1]) {
        default:
          return super.resolveJsonPointer(pointer, index);
      }
    }
  }
  eq B.resolveJsonPointer(String[] pointer, int index) {
    if (pointer.length == 0 || pointer.length == index) {
      return this;
    } else if (pointer.length == index + 1) {
      throw new RuntimeException("there is only one child called " + pointer[index]);
    } else {
      switch (pointer[index + 1]) {
        default:
          return super.resolveJsonPointer(pointer, index);
      }
    }
  }
  eq C.resolveJsonPointer(String[] pointer, int index) {
    if (pointer.length == 0 || pointer.length == index) {
      return this;
    } else if (pointer.length == index + 1) {
      throw new RuntimeException("there is only one child called " + pointer[index]);
    } else {
      switch (pointer[index + 1]) {
        case "D1":
          return getD1().resolveJsonPointer(pointer, index + 2);
        case "D2":
          return getD2().resolveJsonPointer(pointer, index + 2);
        case "D3":
          return getD3(Integer.valueOf(pointer[index + 2])).resolveJsonPointer(pointer, index + 3);
        default:
          return super.resolveJsonPointer(pointer, index);
      }
    }
  }
  eq D.resolveJsonPointer(String[] pointer, int index) {
    if (pointer.length == 0 || pointer.length == index) {
      return this;
    } else if (pointer.length == index + 1) {
      throw new RuntimeException("there is only one child called " + pointer[index]);
    } else {
      switch (pointer[index + 1]) {
        default:
          return super.resolveJsonPointer(pointer, index);
      }
    }
  }
  eq NamedElement.resolveJsonPointer(String[] pointer, int index) {
    if (pointer.length == 0 || pointer.length == index) {
      return this;
    } else if (pointer.length == index + 1) {
      throw new RuntimeException("there is only one child called " + pointer[index]);
    } else {
      switch (pointer[index + 1]) {
        default:
          return super.resolveJsonPointer(pointer, index);
      }
    }
  }
}
