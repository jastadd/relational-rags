package org.jastadd.relast.tests;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

class TestHelpers {

  static int exec(Class klass, String[] args, File err) throws IOException,
      InterruptedException {
    String javaHome = System.getProperty("java.home");
    String javaBin = javaHome + File.separator + "bin" + File.separator + "java";
    String classpath = System.getProperty("java.class.path");
    String className = klass.getName();

    String[] newArgs = new String[args.length + 4];
    newArgs[0] = javaBin;
    newArgs[1] = "-cp";
    newArgs[2] = classpath;
    newArgs[3] = className;
    System.arraycopy(args, 0, newArgs, 4, args.length);

    ProcessBuilder builder = new ProcessBuilder(newArgs);
    builder.redirectError(err);

    Process process = builder.start();
    process.waitFor();
    return process.exitValue();
  }


  static String readFile(String path, Charset encoding)
      throws IOException {
    byte[] encoded = Files.readAllBytes(Paths.get(path));
    return new String(encoded, encoding);
  }

}
