package org.jastadd.relast.tests;

import org.jastadd.relast.compiler.Compiler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.jastadd.relast.tests.TestHelpers.exec;
import static org.jastadd.relast.tests.TestHelpers.readFile;


class Errors {

  private static final String FILENAME_PATTERN = "$FILENAME";

  @Test
  void testStandardErrors() throws IOException {
    test("Errors");
  }

  @Test
  void testStandardErrorsLeft() throws IOException {
    test("ErrorsLeft");
  }

  @Test
  void testInheritance() throws IOException {
    test("Inheritance");
  }

  @Test
  void testInheritanceLeft() throws IOException {
    test("InheritanceLeft");
  }

  @Test
  void testMultipleFiles() throws IOException {
    test("Multiple", "Multiple_1", "Multiple_2");
  }

  @Test
  void testMultipleFilesLeft() throws IOException {
    test("MultipleLeft", "MultipleLeft_1", "MultipleLeft_2");
  }

  private void test(String name, String... inFilenames) throws IOException {
    List<String> inFiles = Arrays.stream(inFilenames.length > 0 ? inFilenames : new String[]{name})
        .map(f -> "./src/test/jastadd/errors/" + f + ".relast")
        .collect(Collectors.toList());
    String outFile = "./src/test/jastadd/errors/" + name + ".out";
    String expectedFile = "./src/test/jastadd/errors/" + name + ".expected";

    try {
      System.out.println("user.dir: " + System.getProperty("user.dir"));
      int returnValue = exec(Compiler.class, inFiles.toArray(new String[0]), new File(outFile));
      Assertions.assertEquals(1, returnValue, "Relast did not return with value 1");
    } catch (IOException | InterruptedException e) {
      e.printStackTrace();
    }

    String out = readFile(outFile, Charset.defaultCharset());
    String expected = readFile(expectedFile, Charset.defaultCharset());
    if (inFiles.size() == 1) {
      expected = expected.replace(FILENAME_PATTERN, inFiles.get(0));
    } else {
      for (int i = 0; i < inFiles.size(); i++) {
        expected = expected.replace(FILENAME_PATTERN + (i + 1), inFiles.get(i));
      }
    }
    List<String> outList = Arrays.asList(out.split("\n"));
    Collections.sort(outList);
    List<String> expectedList = Arrays.stream(expected.split("\n"))
        .sorted()
        .filter(s -> !s.isEmpty() && !s.startsWith("//"))
        .collect(Collectors.toList());

    Assertions.assertLinesMatch(expectedList, outList);

    System.out.println("relast for " + name + " returned \n" + out);
  }


}
