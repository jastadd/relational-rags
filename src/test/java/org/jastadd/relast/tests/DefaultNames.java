package org.jastadd.relast.tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import defaultnames.ast.A;
import defaultnames.ast.B;
import defaultnames.ast.Root;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import static org.jastadd.relast.tests.TestHelpers.readFile;
import static org.junit.jupiter.api.Assertions.*;


@SuppressWarnings({"ArraysAsListWithZeroOrOneArgument", "Duplicates"})
class DefaultNames {
  private Root r;
  private A a1;
  private A a2;
  private A a3;
  private B b1;
  private B b2;
  private B b3;

  @Test
  void doubleRelastRun() throws IOException {

    String firstRun = "./src/test/jastadd/relations/Relations.ast";
    String secondRun = "./src/test/jastadd/relations/Relations2.ast";


    String first = readFile(firstRun, Charset.defaultCharset());
    String second = readFile(secondRun, Charset.defaultCharset());

    Assertions.assertEquals(first, second);
  }

  /**
   * rel A.Di1 -> B;
   */
  @Test
  void testDi1() {
    setup();
    a1.setDi1(b2);
    a2.setDi1(b1);

    assertSame(a1.Di1(), b2);
    assertSame(a2.Di1(), b1);

    a2.setDi1(b2);

    assertSame(a1.Di1(), b2);
    assertSame(a2.Di1(), b2);

    try {
      a3.setDi1(null);
      fail("should throw an exception");
    } catch (Exception e) {
      // OK
    }
  }


  /**
   * rel A.Di2? -> B;
   */
  @Test
  void testDi2() {
    setup();
    a1.setDi2(b2);
    a2.setDi2(b1);

    assertSame(a1.Di2(), b2);
    assertSame(a2.Di2(), b1);

    a2.setDi2(b2);

    assertSame(a1.Di2(), b2);
    assertSame(a2.Di2(), b2);

    a2.clearDi2();

    assertSame(a1.Di2(), b2);
    assertNull(a2.Di2());

    assertTrue(a1.hasDi2());
    assertFalse(a2.hasDi2());
    assertFalse(a3.hasDi2());
  }


  /**
   * rel A.Di3* -> B;
   */
  @Test
  void testDi3() {
    setup();
    a1.addToDi3(b1);
    a1.addToDi3(b2);
    a1.addToDi3(b3);
    a2.addToDi3(b2);

    assertEquals(a1.Di3(), Arrays.asList(b1, b2, b3));
    assertEquals(a2.Di3(), Arrays.asList(b2));
    assertEquals(a3.Di3(), Arrays.asList());

    a1.addToDi3(b1);
    a2.addToDi3(b1);
    a2.addToDi3(b2);

    assertEquals(a1.Di3(), Arrays.asList(b1, b2, b3, b1));
    assertEquals(a2.Di3(), Arrays.asList(b2, b1, b2));
    assertEquals(a3.Di3(), Arrays.asList());

    a1.removeFromDi3(b1);
    a2.removeFromDi3(b2);

    assertEquals(a1.Di3(), Arrays.asList(b2, b3, b1));
    assertEquals(a2.Di3(), Arrays.asList(b1, b2));
    assertEquals(a3.Di3(), Arrays.asList());
  }


  /**
   * rel A.Bi1 <-> B.Bi1;
   */


  @Test
  void testBi11() {
    // Init
    setup();
    a1.setBi1(b1);
    a2.setBi1(b2);

    // Change
    a2.setBi1(b1);

    assertNull(a1.Bi1());
    assertSame(a2.Bi1(), b1);
    assertSame(b1.Bi1(), a2);
    assertNull(b2.Bi1());
  }

  @Test
  void testBi12() {
    // Init
    setup();
    a1.setBi1(b2);

    // Change
    a2.setBi1(b2);

    assertNull(a1.Bi1());
    assertSame(a2.Bi1(), b2);
    assertNull(b1.Bi1());
    assertSame(b2.Bi1(), a2);
  }


  /**
   * rel A.Bi2 <-> B.Bi2?;
   */

  @Test
  void testBi21() {
    // Init
    setup();
    a1.setBi2(b1);
    a2.setBi2(b2);

    // Change
    a2.setBi2(b1);

    assertNull(a1.Bi2());
    assertSame(a2.Bi2(), b1);
    assertSame(b1.Bi2(), a2);
    assertNull(b2.Bi2());
  }

  @Test
  void testBi22() {
    // Init
    setup();
    a1.setBi2(b2);

    // Change
    a2.setBi2(b2);

    assertNull(a1.Bi2());
    assertSame(a2.Bi2(), b2);
    assertNull(b1.Bi2());
    assertSame(b2.Bi2(), a2);
  }


  /**
   * rel A.Bi3 <-> B.Bi3*;
   */
  @Test
  void testBi3() {
    setup();
    a2.setBi3(b2);

    assertNull(a1.Bi3());
    assertSame(a2.Bi3(), b2);
    assertEquals(b1.Bi3(), Arrays.asList());
    assertEquals(b2.Bi3(), Arrays.asList(a2));
    assertEquals(b3.Bi3(), Arrays.asList());

    a2.setBi3(b3);

    assertNull(a1.Bi3());
    assertSame(a2.Bi3(), b3);
    assertEquals(b1.Bi3(), Arrays.asList());
    assertEquals(b2.Bi3(), Arrays.asList());
    assertEquals(b3.Bi3(), Arrays.asList(a2));

    a1.setBi3(b3);
    a3.setBi3(b3);

    assertSame(a1.Bi3(), b3);
    assertSame(a2.Bi3(), b3);
    assertSame(a3.Bi3(), b3);
    assertEquals(b1.Bi3(), Arrays.asList());
    assertEquals(b2.Bi3(), Arrays.asList());
    assertEquals(b3.Bi3(), Arrays.asList(a2, a1, a3));

    a2.setBi3(b1);

    assertSame(a1.Bi3(), b3);
    assertSame(a2.Bi3(), b1);
    assertSame(a3.Bi3(), b3);
    assertEquals(b1.Bi3(), Arrays.asList(a2));
    assertEquals(b2.Bi3(), Arrays.asList());
    assertEquals(b3.Bi3(), Arrays.asList(a1, a3));

    try {
      a2.setBi3(null);
      fail("should throw an exception");
    } catch (Exception e) {
      // OK
    }
  }


  /**
   * rel A.Bi4? <-> B.Bi4;
   */
  @Test
  void testBi41() {
    // Init
    setup();
    a1.setBi4(b1);
    a2.setBi4(b2);

    // Change
    a2.setBi4(b1);

    assertNull(a1.Bi4());
    assertSame(a2.Bi4(), b1);
    assertSame(b1.Bi4(), a2);
    assertNull(b2.Bi4());
  }

  @Test
  void testBi42() {
    // Init
    setup();
    a1.setBi4(b2);

    // Change
    a2.setBi4(b2);

    assertNull(a1.Bi4());
    assertSame(a2.Bi4(), b2);
    assertNull(b1.Bi4());
    assertSame(b2.Bi4(), a2);
  }


  /**
   * rel A.Bi5? <-> B.Bi5?;
   */
  @Test
  void testBi51() {
    // Init
    setup();
    a1.setBi5(b1);
    a2.setBi5(b2);

    // Change
    a2.setBi5(b1);

    assertNull(a1.Bi5());
    assertSame(a2.Bi5(), b1);
    assertSame(b1.Bi5(), a2);
    assertNull(b2.Bi5());
  }

  @Test
  void testBi52() {
    // Init
    setup();
    a1.setBi5(b2);

    // Change
    a2.setBi5(b2);

    assertNull(a1.Bi5());
    assertSame(a2.Bi5(), b2);
    assertNull(b1.Bi5());
    assertSame(b2.Bi5(), a2);
  }


  /**
   * rel A.Bi6? <-> B.Bi6*;
   */
  @Test
  void testBi6() {
    setup();
    a2.setBi6(b2);

    assertNull(a1.Bi6());
    assertSame(a2.Bi6(), b2);
    assertEquals(b1.Bi6(), Arrays.asList());
    assertEquals(b2.Bi6(), Arrays.asList(a2));
    assertEquals(b3.Bi6(), Arrays.asList());

    a2.setBi6(b3);

    assertNull(a1.Bi6());
    assertSame(a2.Bi6(), b3);
    assertEquals(b1.Bi6(), Arrays.asList());
    assertEquals(b2.Bi6(), Arrays.asList());
    assertEquals(b3.Bi6(), Arrays.asList(a2));

    a1.setBi6(b3);
    a3.setBi6(b3);

    assertSame(a1.Bi6(), b3);
    assertSame(a2.Bi6(), b3);
    assertSame(a3.Bi6(), b3);
    assertEquals(b1.Bi6(), Arrays.asList());
    assertEquals(b2.Bi6(), Arrays.asList());
    assertEquals(b3.Bi6(), Arrays.asList(a2, a1, a3));

    a2.setBi6(b1);

    assertSame(a1.Bi6(), b3);
    assertSame(a2.Bi6(), b1);
    assertSame(a3.Bi6(), b3);
    assertEquals(b1.Bi6(), Arrays.asList(a2));
    assertEquals(b2.Bi6(), Arrays.asList());
    assertEquals(b3.Bi6(), Arrays.asList(a1, a3));

    a2.clearBi6();

    assertSame(a1.Bi6(), b3);
    assertNull(a2.Bi6());
    assertSame(a3.Bi6(), b3);
    assertEquals(b1.Bi6(), Arrays.asList());
    assertEquals(b2.Bi6(), Arrays.asList());
    assertEquals(b3.Bi6(), Arrays.asList(a1, a3));

    assertTrue(a1.hasBi6());
    assertFalse(a2.hasBi6());
    assertTrue(a3.hasBi6());
  }


  /**
   * rel A.Bi7* <-> B.Bi7;
   */
  @Test
  void testBi7() {
    setup();
    a2.addToBi7(b2);

    assertEquals(a1.Bi7(), Arrays.asList());
    assertEquals(a2.Bi7(), Arrays.asList(b2));
    assertNull(b1.Bi7());
    assertSame(b2.Bi7(), a2);
    assertNull(b3.Bi7());

    a2.addToBi7(b3);
    a1.addToBi7(b2);

    assertEquals(a1.Bi7(), Arrays.asList(b2));
    assertEquals(a2.Bi7(), Arrays.asList(b3));
    assertNull(b1.Bi7());
    assertSame(b2.Bi7(), a1);
    assertSame(b3.Bi7(), a2);

    a1.addToBi7(b1);

    assertEquals(a1.Bi7(), Arrays.asList(b2, b1));
    assertEquals(a2.Bi7(), Arrays.asList(b3));
    assertSame(b1.Bi7(), a1);
    assertSame(b2.Bi7(), a1);
    assertSame(b3.Bi7(), a2);

    a1.addToBi7(b1);

    assertEquals(a1.Bi7(), Arrays.asList(b2, b1));
    assertEquals(a2.Bi7(), Arrays.asList(b3));
    assertSame(b1.Bi7(), a1);
    assertSame(b2.Bi7(), a1);
    assertSame(b3.Bi7(), a2);

    a1.removeFromBi7(b1);

    assertEquals(a1.Bi7(), Arrays.asList(b2));
    assertEquals(a2.Bi7(), Arrays.asList(b3));
    assertNull(b1.Bi7());
    assertSame(b2.Bi7(), a1);
    assertSame(b3.Bi7(), a2);
  }


  /**
   * rel A.Bi8* <-> B.Bi8?;
   */
  @Test
  void testBi8() {
    setup();
    a2.addToBi8(b2);

    assertEquals(a1.Bi8(), Arrays.asList());
    assertEquals(a2.Bi8(), Arrays.asList(b2));
    assertNull(b1.Bi8());
    assertSame(b2.Bi8(), a2);
    assertNull(b3.Bi8());

    a2.addToBi8(b3);
    a1.addToBi8(b2);

    assertEquals(a1.Bi8(), Arrays.asList(b2));
    assertEquals(a2.Bi8(), Arrays.asList(b3));
    assertNull(b1.Bi8());
    assertSame(b2.Bi8(), a1);
    assertSame(b3.Bi8(), a2);

    a1.addToBi8(b1);

    assertEquals(a1.Bi8(), Arrays.asList(b2, b1));
    assertEquals(a2.Bi8(), Arrays.asList(b3));
    assertSame(b1.Bi8(), a1);
    assertSame(b2.Bi8(), a1);
    assertSame(b3.Bi8(), a2);

    a1.addToBi8(b1);

    assertEquals(a1.Bi8(), Arrays.asList(b2, b1));
    assertEquals(a2.Bi8(), Arrays.asList(b3));
    assertSame(b1.Bi8(), a1);
    assertSame(b2.Bi8(), a1);
    assertSame(b3.Bi8(), a2);

    a1.removeFromBi8(b1);

    assertEquals(a1.Bi8(), Arrays.asList(b2));
    assertEquals(a2.Bi8(), Arrays.asList(b3));
    assertNull(b1.Bi8());
    assertSame(b2.Bi8(), a1);
    assertSame(b3.Bi8(), a2);
  }


  /**
   * rel A.Bi9* <-> B.Bi9*;
   */
  @Test
  void testBi9() {
    setup();
    a1.addToBi9(b1);
    a1.addToBi9(b2);

    assertEquals(a1.Bi9(), Arrays.asList(b1, b2));
    assertEquals(a2.Bi9(), Arrays.asList());
    assertEquals(a3.Bi9(), Arrays.asList());
    assertEquals(b1.Bi9(), Arrays.asList(a1));
    assertEquals(b2.Bi9(), Arrays.asList(a1));
    assertEquals(b3.Bi9(), Arrays.asList());

    b3.addToBi9(a1);
    b3.addToBi9(a3);
    b3.addToBi9(a1);

    assertEquals(a1.Bi9(), Arrays.asList(b1, b2, b3, b3));
    assertEquals(a2.Bi9(), Arrays.asList());
    assertEquals(a3.Bi9(), Arrays.asList(b3));
    assertEquals(b1.Bi9(), Arrays.asList(a1));
    assertEquals(b2.Bi9(), Arrays.asList(a1));
    assertEquals(b3.Bi9(), Arrays.asList(a1, a3, a1));

    b3.removeFromBi9(a1);

    assertEquals(a1.Bi9(), Arrays.asList(b1, b2, b3));
    assertEquals(a2.Bi9(), Arrays.asList());
    assertEquals(a3.Bi9(), Arrays.asList(b3));
    assertEquals(b1.Bi9(), Arrays.asList(a1));
    assertEquals(b2.Bi9(), Arrays.asList(a1));
    assertEquals(b3.Bi9(), Arrays.asList(a3, a1));
  }


  @Test
  void testImmutableList() {
    setup();

    a1.addToDi3(b1);
    a1.addToDi3(b2);
    try {
      a1.Di3().add(b3);
      fail("should throw an exception");
    } catch (Exception e) {
      // OK
    }

    a1.addToBi7(b1);
    a1.addToBi7(b2);
    try {
      a1.Bi7().add(b3);
      fail("should throw an exception");
    } catch (Exception e) {
      // OK
    }

    a1.addToBi9(b1);
    a1.addToBi9(b2);
    try {
      a1.Bi9().add(b3);
      fail("should throw an exception");
    } catch (Exception e) {
      // OK
    }
  }

  @BeforeEach
  void setup() {
    r = new Root();
    a1 = new A("a1");
    a2 = new A("a2");
    a3 = new A("a3");
    b1 = new B("b1");
    b2 = new B("b2");
    b3 = new B("b3");

    r.addA(a1);
    r.addA(a2);
    r.addA(a3);
    r.addB(b1);
    r.addB(b2);
    r.addB(b3);
  }
}
