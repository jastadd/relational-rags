package org.jastadd.relast.tests;

import constructors.ast.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

/**
 * Testing generated constructors.
 *
 * @author rschoene - Initial contribution
 */
@SuppressWarnings("unused")
public class ConstructorsTest {

  @Test
  public void testX() {
    X x = new X();
    Assertions.assertEquals(1, numberOfConstructors(X.class));
  }

  @Test
  public void testY() {
    Y y0 = new Y();
    Y y1 = new Y(new S());
    Assertions.assertEquals(2, numberOfConstructors(Y.class));
  }

  @Test
  public void testA() {
    A a0 = new A();
    A a1 = new A(new X());
    Assertions.assertEquals(2, numberOfConstructors(A.class));
  }

  @Test
  public void testS() {
    A a = new A();
    S s0 = new S();
    S s1 = new S(a, new List<>(), new Opt<>());

    // S1:A S2:A* [S3:A]       S.r2*
    // \------ S ------/  \- rel of S -/
    S s2 = new S(a, new List<>(), new Opt<>(), new ArrayList<>());
    Assertions.assertEquals(3, numberOfConstructors(S.class));
  }

  @Test
  public void testB() {
    A a = new A();
    B b0 = new B();
    B b1 = new B(a, new List<>(), new Opt<>(), a, new List<>(), new Opt<>());

    // S1:A S2:A* [S3:A]       S.r2*     B1:A B2:A* [B3:A]     B.r3?
    // \------ S ------/  \- rel of S -/ \------ B ------/ \- rel of B -/
    B b2 = new B(a, new List<>(), new Opt<>(), new ArrayList<>(), a, new List<>(), new Opt<>(), null);
    Assertions.assertEquals(3, numberOfConstructors(B.class));
  }

  @Test
  public void testC() {
    A a = new A();
    C c0 = new C();
    C c1 = new C(a, new List<>(), new Opt<>(), new Opt<>(), new List<>(), a);

    // S1:A S2:A* [S3:A]       S.r2*         B.r3?      [B3:A] B2:A* B1:A
    // \------ S ------/  \- rel of S -/ \- rel of B -/ \------ C ------/
    C c2 = new C(a, new List<>(), new Opt<>(), new ArrayList<>(), null, new Opt<>(), new List<>(), a);
    Assertions.assertEquals(3, numberOfConstructors(B.class));
  }

  private int numberOfConstructors(Class<?> clazz) {
    return clazz.getConstructors().length;
  }
}
