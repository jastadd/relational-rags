package org.jastadd.relast.tests;

import org.junit.jupiter.api.Test;
import serializer.ast.*;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.Period;

import static org.assertj.core.api.Assertions.assertThat;


@SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
class Serializer {


  @Test
  void testDi1() throws SerializationException, DeserializationException, IOException {

    Root r = new Root();
    A a1 = new A("a1");
    A a2 = new A("a2");
    A a3 = new A("a3");
    B b1 = new B("b1");
    B b2 = new B("b2");
    B b3 = new B("b3");
    C c = new C();
    c.setName("c");

    // non-terminals
    c.setD1(new D("d1"));
    c.setD2(new D("d2"));
    c.addD3(new D("D3-1"));
    c.addD3(new D("D3-2"));
    c.addD3(new D("D3-3"));

    // tokens
    c.setF1(42f);
    c.setF2(42.13f);

    c.setDD1(42.d);
    c.setDD2(42.13d);

    c.setI1(23);
    c.setI2(42);
    c.setS1((short)66);
    c.setS2((short)77);

    c.setL1(342l);
    c.setL2(5453L);

    c.setB1((byte)5);
    c.setB2((byte)'c');

    c.setO1(true);
    c.setO2(false);

    c.setC1('c');
    c.setC2((char)5);

    c.setT1("23");
    c.setT2("dfjsv");

    c.setN(Instant.now());
    c.setP(Period.ofDays(1));

    c.setDay(Weekday.FRIDAY);

    r.setC(c);

    // non-containment relations
    r.addA(a1);
    r.addA(a2);
    r.addA(a3);
    r.addB(b1);
    r.addB(b2);
    r.addB(b3);

    // Di1
    a1.setDi1(b2);
    a2.setDi1(b1);
    a3.setDi1(b3);

    // Di2
    a1.setDi2(b2);
    a3.setDi2(b1);

    // Di3
    a1.addDi3(b1);
    a1.addDi3(b2);
    a1.addDi3(b3);
    a2.addDi3(b2);

    // Bi1
    a1.setBi1(b3);
    a2.setBi1(b2);
    a3.setBi1(b1);

    // Bi2
    a1.setBi2(b1);
    a2.setBi2(b2);
    a3.setBi2(b3);

    // Bi3
    a1.setBi3(b2);
    a2.setBi3(b2);
    a3.setBi3(b2);

    // Bi5
    a1.setBi5(b1);
    a2.setBi5(b3);

    // Bi6
    a2.setBi6(b3);
    a3.setBi6(b3);

    // Bi9
    a1.addBi9(b1);
    a1.addBi9(b3);
    a2.addBi9(b3);

    File f1a = File.createTempFile("original", ".json");
    System.out.println(f1a.getAbsoluteFile());
    r.serialize(f1a);

    Root copy = Root.deserialize(f1a);
    File f1b = File.createTempFile("copy", ".json");
    copy.serialize(f1b);

    assertThat(f1b).hasSameContentAs(f1a);

    // remove a2
    a1.setDi1(b3);
    a1.setDi2(b3);
    a1.removeDi3(b2);
    a1.removeDi3(b2);
    a1.setBi3(b1);
    a3.setBi3(b1);
    b3.clearBi5();
    b3.removeBi6(a2);
    b3.removeBi9(a2);
    r.getAList().removeChild(r.getAList().getIndexOfChild(a2));
    r.getBList().removeChild(r.getBList().getIndexOfChild(b2));

    File f2a = File.createTempFile("original", ".json");
    System.out.println(f2a.getAbsoluteFile());
    r.serialize(f2a);

    copy = Root.deserialize(f2a);
    File f2b = File.createTempFile("copy", ".json");
    copy.serialize(f2b);

    assertThat(f2b).hasSameContentAs(f2a);

  }

}
