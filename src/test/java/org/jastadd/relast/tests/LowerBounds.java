package org.jastadd.relast.tests;

import lowerbounds.ast.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LowerBounds {

  /*
   * Root ::= A* B*;
   * A ::= <Name> [C];
   * B ::= <Name>;
   * C ::= <Name>;
   * rel A.Br -> B;
   * rel B.Cr <-> C.Br;
   * rel Root.Aa? -> A;
   */
  @Test
  void test() {
    Root r = new Root();
    C c1 = new C("c1");
    C c2 = new C("c2");
    A a1 = new A("a1", new Opt<>(c1));
    A a2 = new A("a2", new Opt<>(c2));
    B b1 = new B("b1");
    B b2 = new B("b2");
    r.addA(a1);
    r.addA(a2);
    r.addB(b1);
    r.addB(b2);

    assertTrue(r.violatesLowerBounds());

    a1.setBr(b1);
    a2.setBr(b2);
    b1.setCr(c1);
    b2.setCr(c2);

    assertFalse(r.violatesLowerBounds());

    b2.setCr(c1);

    assertTrue(r.violatesLowerBounds());

    b1.setCr(c2);

    assertFalse(r.violatesLowerBounds());
  }
}
