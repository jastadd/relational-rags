package org.jastadd.relast.tests;

import org.junit.jupiter.api.Test;
import pointer.serializer.ast.*;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.Period;

import static org.assertj.core.api.Assertions.assertThat;


class SerializerPointer {


  @Test
  void testDi1() throws SerializationException, DeserializationException, IOException {

    Root r = new Root();
    A a1 = new A("a1");
    A a2 = new A("a2");
    A a3 = new A("a3");
    B b1 = new B("b1");
    B b2 = new B("b2");
    B b3 = new B("b3");
    C c = new C();
    c.setName("c");

    // non-terminals

    D d1 = new D("d1");

    c.setD1(d1);
    c.setD2(new D("d2"));
    c.addD3(new D("D3-1"));
    c.addD3(new D("D3-2"));
    c.addD3(new D("D3-3"));

    r.setC(c);

    // non-containment relations
    r.addA(a1);
    r.addA(a2);
    r.addA(a3);
    r.addB(b1);
    r.addB(b2);
    r.addB(b3);

    // Di1
    a1.setDi1(b2);
    a2.setDi1(b1);
    a3.setDi1(b3);

    // Di2
    a1.setDi2(b2);
    a3.setDi2(b1);

    // Di3
    a1.addToDi3(b1);
    a1.addToDi3(b2);
    a1.addToDi3(b3);
    a2.addToDi3(b2);

    // Bi1
    a1.setBi1(b3);
    a2.setBi1(b2);
    a3.setBi1(b1);

    // Bi2
    a1.setBi2(b1);
    a2.setBi2(b2);
    a3.setBi2(b3);

    // Bi3
    a1.setBi3(b2);
    a2.setBi3(b2);
    a3.setBi3(b2);

    // Bi5
    a1.setBi5(b1);
    a2.setBi5(b3);

    // Bi6
    a2.setBi6(b3);
    a3.setBi6(b3);

    // Bi9
    a1.addToBi9(b1);
    a1.addToBi9(b3);
    a2.addToBi9(b3);

    // D
    r.setD(d1);

    File f1a = File.createTempFile("original", ".json");
    System.out.println(f1a.getAbsoluteFile());
    r.serialize(f1a);

    com.fasterxml.jackson.core.JsonFactory factory = new com.fasterxml.jackson.core.JsonFactory();
    com.fasterxml.jackson.core.JsonGenerator generator = factory.createGenerator(System.out, com.fasterxml.jackson.core.JsonEncoding.UTF8);
    generator.setPrettyPrinter(new com.fasterxml.jackson.core.util.DefaultPrettyPrinter());
    r.serialize(generator);
    generator.close();

    Root copy = Root.deserialize(f1a);
    File f1b = File.createTempFile("copy", ".json");
    copy.serialize(f1b);

    assertThat(f1b).hasSameContentAs(f1a);

    // remove a2
    a1.setDi1(b3);
    a1.setDi2(b3);
    a1.removeFromDi3(b2);
    a1.removeFromDi3(b2);
    a1.setBi3(b1);
    a3.setBi3(b1);
    b3.clearBi5();
    b3.removeFromBi6(a2);
    b3.removeFromBi9(a2);
    r.getAList().removeChild(r.getAList().getIndexOfChild(a2));
    r.getBList().removeChild(r.getBList().getIndexOfChild(b2));

    File f2a = File.createTempFile("original", ".json");
    System.out.println(f2a.getAbsoluteFile());
    r.serialize(f2a);

    copy = Root.deserialize(f2a);
    File f2b = File.createTempFile("copy", ".json");
    copy.serialize(f2b);

    assertThat(f2b).hasSameContentAs(f2a);

  }

}
