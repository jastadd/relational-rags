# Process to contribute new features or to fix bugs

To propose a new feature, or to report a bug, first [create an issue][create-issue] and add labels accordingly.
Working on such issues is done by creating a merge request from the issue page, which 1) creates a new branch to work on, and 2) creates a new WIP merge request for the new branch.
Once done (and new tests are written to ensure, a bug is really fixed, and the feature does the right thing), the merge request will be accepted and merged into `develop`.

# Creating normal test cases

A test usually defines a grammar, optionally some attributes and a Test class.
It then normally runs RelAST followed by JastAdd.
To create a new test, the following four ingredients are involved:

## Specification

Here, the grammar and attributes to be compiled are contained.
Use `src/test/jastadd/$YourTestName` as directory to put your files into.
All generated `*.ast`, `*.jadd` and `ResolverStubs.jrag` are already ignored by the main `.gitignore`.

## Generated Java files

Usually, all files are generated into a separate directory in `src/test/java-gen/`.
This location can be [configured](#build-configuration).

## Build configuration

Currently, the test setup is configured within `build.gradle` using a specialized Gradle task `RelastTest` provided by the [preprocessor testing Gradle plugin][preprocessor-testing].
An example configuration might look like:

```groovy
task compileMultipleTest(type: RelastTest) {
    relast {
        inputFiles = [file('src/test/jastadd/multiple/Part1.relast'),
                      file('src/test/jastadd/multiple/Part2.relast'),
                      file('src/test/jastadd/multiple/Part3.relast')]
        grammarName = 'src/test/jastadd/multiple/Multiple'
        useJastAddNames = true
        noResolverHelper = true
    }
    jastadd {
        packageName = 'multiple.ast'
        inputFiles = [file('src/test/jastadd/Utils.jadd')]
    }
}
```

Please see the [documentation of the plugin][preprocessor-testing] for all options.

## Test files

The actual test source, usually located in `src/test/java/` in the package `org.jastadd.relast.tests`.
Pay attention to import the correct nonterminal classes by using the same package as [configured](#build-configuration).
This project uses JUnit 5 as test framework.

# Creating special tests

Aside from the [normal tests](#creating-normal-test-cases), there are some special tests.

## Negative parser tests

To check, errors are found and contain the correct messages, one test [`Errors`][Errors.java] is used.
Here, the RelAST compiler is invoked manually, and the actual error messages are compared to expected ones for each grammar in `src/test/jastadd/errors`.
The expected messages can contain the special word `$FILENAME` to refer to the filename of the grammar, if there is only one, or `$FILENAME1`, `$FILENAME2` etc., if there are many.
Furthermore, empty lines, lines starting with `//` and the order of the error messages are ignored.

## Output diff

Currently, there is one test to test whether the output of RelAST is a valid input for RelAST and produces the same output again.
To achieve this, there are two Gradle tasks. The first produces the usual `.ast` and `.jadd` files, whereas the second task takes the `.ast` as input.
The test then ensures, that both output grammars are identical.

# Releases and Publishing (Maintainer only)

Important information:

- Currently, we are publishing to a private Nexus Maven repository only.
- We are using [git-flow][git-flow], so only new merge requests are considered for releases to appear in the `master` branch.
- The version is set in the configuration file [RelASTVersion.properties][RelASTVersion.properties].

The workflow:

1) Finish your work with the current feature(s) and merge those back in `develop`.
1) Choose a new version number `$nextVersion` depending on the introduced changes **following [semantic versioning][semantic-versioning]**.
1) Create a new release branch named `release/$nextVersion` and switch to this branch.
1) Set the version number in the config file calling `./gradlew newVersion -Pvalue=$nextVersion`
1) Commit this change.
1) (Optional) Build a new jar file calling `./gradlew jar` (this is automatically called in the publish step and only used to test the newly set version number)
1) Check, if everything works as planned, e.g., version number is picked up when running the application with `--version`, and all test succeed.
1) Merge the release branch into `master` (using a merge request) and also back into `develop`.
1) Delete the release branch.
1) [Create a new release][create-release]. Choose the following:
    - *Tag name*: the chosen version number
    - *Create from*: leave the default `master`
    - *Message*: "Version " and the chose version number
    - *Release notes*: list the (important) changes compared to the last release, prepend a link to the built jar using the line `[:floppy_disk: publish-relast-poc-$nextVersion.jar](/../../../-/jobs/$jobNumber/artifacts/raw/build/libs/publish-relast-poc-$nextVersion.jar?inline=false)` replacing `$jobNumber` with the `jar` job of the pipeline run after the merge request, and `$nextVersion`
1) Publish the built jar to the maven repository calling `./gradlew publish`

[git-flow]: https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
[Errors.java]: /../blob/master/src/test/java/org/jastadd/relast/tests/Errors.java
[RelASTVersion.properties]: /../-/blob/master/src/main/resources/RelASTVersion.properties
[semantic-versioning]: https://semver.org/
[create-release]: /../-/tags/new
[create-issue]: https://git-st.inf.tu-dresden.de/jastadd/relational-rags/issues/new
[preprocessor-testing]: https://jastadd.pages.st.inf.tu-dresden.de/testing/
