.. Relational RAGs documentation master file, created by
   sphinx-quickstart on Tue Oct 20 11:41:47 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Relational RAGs documentation
=============================

.. toctree::
   :caption: Contents:
   :hidden:

   Using.rst
   Command-Line-Arguments.rst
   Supported-Relations.rst
   publications.rst

.. image:: images/relast-process.png
  :alt: RelAST process

See `releases page`_ for the latest version.

.. _releases page: https://git-st.inf.tu-dresden.de/jastadd/relational-rags/-/releases
