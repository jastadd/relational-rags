Using Relational RAGs
=====================

The Relational RAGs preprocessor takes a ``.relast`` file as input comprising AST rules and relations. It produces files that afterwards are processed by JastAdd_ to generated Java code.
To use it in your project, build the JAR file running

.. code:: Bash

  ./gradlew jar

This will produce ``build/libs/relast.jar``. Copy it to your project and extend your build config (preferably Gradle_) with the following:

.. code:: Groovy

  task preprocess(type: JavaExec) {
      group = 'Build'
      main = "-jar"
      args = [
              "libs/relast.jar",
              "./src/main/jastadd/main.relast",
              "--listClass=RefList",
              "--jastAddList=JastAddList",
              "--file"
      ]

      inputs.files file("./src/main/jastadd/main.relast")
      outputs.files file("./src/main/jastadd/mainGen.ast"), file("./src/main/jastadd/mainGen.jadd")
  }

Note that you may have to change

- the directory of ``relast.jar``
- the ``.relast`` file(s) both as argument(s) and input file(s)
- the output files
- parameters of RelAST (in this case, the list classes were renamed), the following table shows the supported ones

.. _JastAdd: http://jastadd.org/
.. _Gradle: https://gradle.org/
