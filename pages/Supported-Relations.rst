Supported relations
===================

.. code:: Java

  // Directed relations
  A.b    -> B;
  A.b?   -> B;
  A.bs*  -> B;
  B      <- A.b  ;
  B      <- A.b? ;
  B      <- A.bs*;

  // Bidirectional relations
  A.b   <-> B.a;
  A.b   <-> B.a?;
  A.b   <-> B.as*;
  A.b?  <-> B.a;
  A.b?  <-> B.a?;
  A.b?  <-> B.as*;
  A.bs* <-> B.a;
  A.bs* <-> B.a?;
  A.bs* <-> B.as*;
