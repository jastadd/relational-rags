Publications
============

Main publication at SLE'18
--------------------------

.. raw:: html

  <link rel="stylesheet"
      href="https://dl.acm.org/specs/products/acm/widgets/authorizer/scss/style.css" />
  <style>
  .multi-search {
    min-height: 0;
    padding: 0px;
  }
  .issue-item {
  }
  </style>
  <div class="multi-search multi-search--issue-item">
              <div class="issue-item clearfix">
                  <div class="issue-item__citation">
                      <div class="issue-heading">chapter</div>
                  </div>
                  <div class="issue-item__content">
                      <h5 class="issue-item__title"><a
                              href="https://dl.acm.org/doi/10.1145/3276604.3276616?cid=81318498502">Continuous
                              model validation using reference attribute
                              grammars</a></h5>

                      <ul class="rlist--inline loa truncate-list"
                          title="list of authors" data-lines="2">
                          <li><a href="https://dl.acm.org/profile/99659180755"
                                  title="Johannes Mey"><img class="author-picture"
                                      src="https://dl.acm.org/pb-assets/icons/DOs/default-profile-1543932446943.svg"
                                      alt="Johannes Mey profile image" /><span>Johannes
                                      Mey</span></a><span
                                  class="loa_author_inst hidden">
                                  <p data-doi="10.1145/contrib-99659180755">TU
                                      Dresden, Germany</p>
                              </span><span>, </span></li>
                          <li><a href="https://dl.acm.org/profile/81318498502"
                                  title="René Schöne"><img class="author-picture"
                                      src="https://dl.acm.org/do/10.1145/contrib-81318498502/rel-imgonly/author-rene.jpg"
                                      alt="René Schöne profile image" /><span>René
                                      Schöne</span></a><span
                                  class="loa_author_inst hidden">
                                  <p data-doi="10.1145/contrib-81318498502">TU
                                      Dresden, Germany</p>
                              </span><span>, </span></li>
                          <li><a href="https://dl.acm.org/profile/81100270533"
                                  title="Görel Hedin"><img class="author-picture"
                                      src="https://dl.acm.org/do/10.1145/contrib-81100270533/rel-imgonly/81100270533.jpg"
                                      alt="Görel Hedin profile image" /><span>Görel
                                      Hedin</span></a><span
                                  class="loa_author_inst hidden">
                                  <p data-doi="10.1145/contrib-81100270533">Lund
                                      University, Sweden</p>
                              </span><span>, </span></li>
                          <li><a href="https://dl.acm.org/profile/81482659289"
                                  title="Emma Söderberg"><img
                                      class="author-picture"
                                      src="https://dl.acm.org/pb-assets/icons/DOs/default-profile-1543932446943.svg"
                                      alt="Emma Söderberg profile image" /><span>Emma
                                      Söderberg</span></a><span
                                  class="loa_author_inst hidden">
                                  <p data-doi="10.1145/contrib-81482659289">Lund
                                      University, Sweden</p>
                              </span><span>, </span></li>
                          <li><a href="https://dl.acm.org/profile/81100443648"
                                  title="Thomas Kühn"><img class="author-picture"
                                      src="https://dl.acm.org/pb-assets/icons/DOs/default-profile-1543932446943.svg"
                                      alt="Thomas Kühn profile image" /><span>Thomas
                                      Kühn</span></a><span
                                  class="loa_author_inst hidden">
                                  <p data-doi="10.1145/contrib-81100443648">TU
                                      Dresden, Germany</p>
                              </span><span>, </span></li>
                          <li><a href="https://dl.acm.org/profile/82459076457"
                                  title="Niklas Fors"><img class="author-picture"
                                      src="https://dl.acm.org/pb-assets/icons/DOs/default-profile-1543932446943.svg"
                                      alt="Niklas Fors profile image" /><span>Niklas
                                      Fors</span></a><span
                                  class="loa_author_inst hidden">
                                  <p data-doi="10.1145/contrib-82459076457">Lund
                                      University, Sweden</p>
                              </span><span>, </span></li>
                          <li><a href="https://dl.acm.org/profile/82858857457"
                                  title="Jesper Öqvist"><img
                                      class="author-picture"
                                      src="https://dl.acm.org/pb-assets/icons/DOs/default-profile-1543932446943.svg"
                                      alt="Jesper Öqvist profile image" /><span>Jesper
                                      Öqvist</span></a><span
                                  class="loa_author_inst hidden">
                                  <p data-doi="10.1145/contrib-82858857457">Lund
                                      University, Sweden</p>
                              </span><span>, </span></li>
                          <li><a href="https://dl.acm.org/profile/81100444066"
                                  title="Uwe Aßmann"><img class="author-picture"
                                      src="https://dl.acm.org/pb-assets/icons/DOs/default-profile-1543932446943.svg"
                                      alt="Uwe Aßmann profile image" /><span>Uwe
                                      Aßmann</span></a><span
                                  class="loa_author_inst hidden">
                                  <p data-doi="10.1145/contrib-81100444066">TU
                                      Dresden, Germany</p>
                              </span></li>
                      </ul>

                      <div class="issue-item__detail"><span>October
                              2018</span><span class="dot-separator">pp 70-82
                          </span><span><a
                                  href="https://doi.org/10.1145/3276604.3276616"
                                  class="issue-item__doi  dot-separator">https://doi.org/10.1145/3276604.3276616</a></span>
                      </div>
                      <div data-lines='4'
                          class="issue-item__abstract truncate-text">
                          <div class="issue-item__abstract truncate-text"
                              data-lines="4">

                              <p>Just like current software systems, models are
                                  characterised by increasing complexity
                                  and rate of change. Yet, these models only
                                  become useful if they can be continuously
                                  evaluated and validated. To achieve sufficiently
                                  low response times for large ...
                              </p>
                          </div>
                      </div>
                  </div>
              </div>
  </div>
  <br/>

Follow-Up publication in JCL
----------------------------

**Relational reference attribute grammars: Improving continuous model validation**

Johannes Mey |a|, René Schöne |a|, Görel Hedin |b|, Emma Söderberg |b|, Thomas Kühn |a|, Niklas Fors |b|, Jesper Öqvist |b|, Uwe Aßmann |a| 

- |a| Technische Universitãt Dresden, Germany
- |b| Lund University, Sweden

Received 31 March 2019, Revised 1 November 2019, Accepted 20 December 2019, Available online 20 January 2020.

https://doi.org/10.1016/j.cola.2019.100940

.. |a| replace:: :sup:`a`
.. |b| replace:: :sup:`b`


Publications using Relational RAGs
----------------------------------

- `Subpage on RagConnect`_
- Götz, Sebastian, Johannes Mey, René Schöne, and Uwe Aßmann. **“A JastAdd- and ILP-Based Solution to the Software-Selection and Hardware-Mapping-Problem at the TTC 2018.”** In 11th Transformation Tool Contest, 2018.
- Götz, Sebastian, Johannes Mey, René Schöne, and Uwe Aßmann. **“Quality-Based Software-Selection and Hardware-Mapping as Model Transformation Problem.”** In 11th Transformation Tool Contest, 2018.
- Mey, Johannes, René Schöne, Christopher Werner, and Uwe Aßmann. **“Transforming Truth Tables to Binary Decision Diagrams Using Relational Reference Attribute Grammars.”** In Proceedings of the 12th Transformation Tool Contest (TTC  2019), 2019.
- Schöne, René, and Johannes Mey. **“A JastAdd-Based Solution to the TTC 2018 Social Media Case.”** In 11th Transformation Tool Contest, 2018.
- Schöne, René, Johannes Mey, Boqi Ren, and Uwe Aßmann. **“Bridging the Gap between Smart Home Platforms and Machine Learning Using Relational Reference Attribute Grammars.”** In Proceedings of the 14th International Workshop on Models\@run.Time, 533–42. Munich, 2019. https://doi.org/10.1109/MODELS-C.2019.00083
- Mey, Johannes, Thomas Kühn, René Schöne, and Uwe Aßmann. **“Reusing Static Analysis across Different Domain-Specific Languages Using Reference Attribute Grammars.”** Programming 4, no. 3 (February 17, 2020). https://doi.org/10.22152/programming-journal.org/2020/4/15


.. _Subpage on RagConnect: http://connector.relational-rags.eu/
