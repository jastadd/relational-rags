Supported command-line options
==============================

--ast                   print AST (ignores quiet option)
--file                  write output to files <filename>Gen.ast and <filename>Gen.jadd
--grammarName           name of the generated grammar and aspect (without file extension)
--jastAddList           set the name of the List type in JastAdd (has to match the option '--List' or its default List)
--listClass             determine the class name of the nonterminal reference list
--quiet                 do not output anything on stdout
--resolverHelper        create a subtype for each type containing a string that can be used to resolve the type later
--serializer            generate a (de-)serializer (allowed values: jackson, jackson-json-pointer, jackson-manual-references)
--useJastAddNames       generate names in the form of addX, removeX and setX. If omitted, the default, original naming scheme resulting in addToX, removeFromX and setX will be used.
--version               print version and exit
