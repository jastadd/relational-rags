# RelAST Preprocessor

![RelAST process](relast-process.png)

See [releases page](/../../releases) for the latest version and <https://jastadd.pages.st.inf.tu-dresden.de/relational-rags/> for more documentation.

The RelAST preprocessor takes a `.relast` file as input comprising AST rules and relations. It produces files that afterwards are processed by JastAdd to generated Java code.
To use it in your project, build the JAR file running

```
./gradlew jar
```

This will produce `build/libs/relast.jar`. Copy it to your project and extend your build config (preferably Gradle) with the following:

```
task preprocess(type: JavaExec) {
    group = 'Build'
    main = "-jar"
    args = [
            "libs/relast.jar",
            "./src/main/jastadd/main.relast",
            "--listClass=RefList",
            "--jastAddList=JastAddList",
            "--file"
    ]

    inputs.files file("./src/main/jastadd/main.relast")
    outputs.files file("./src/main/jastadd/mainGen.ast"), file("./src/main/jastadd/mainGen.jadd")
}
```

Note that you may have to change

- the directory of `relast.jar`
- the `.relast` file(s) both as argument(s) and input file(s)
- the output files
- parameters of RelAST (in this case, the list classes were renamed), the following table shows the supported ones

## Supported command-line options

|         Name        |                                             Description                                              |    Default    |
|---------------------|------------------------------------------------------------------------------------------------------|---------------|
| `--ast`             | Print AST (ignores `--quiet` option)                                                                 | Do not print  |
| `--grammarName`     | Directory and file prefix for the generated grammar and jrag created by RelAST.                      | `Grammar`     |
| `--jastAddList`     | Alternative name for `List` nodes (has to match the option `--List` of JastAdd or its default List). | `List`        |
| `--listClass`       | Set the class name of the nonterminal reference list                                                 | `ArrayList`   |
| `--quiet`           | Do not output anything on stdout.                                                                    | Normal output |
| `--resolverHelper`  | Set to `true` to generate means for lazy resolving.                                                  | `false`       |
| `--serializer`      | Generate a (de-)serializer. One of {`jackson`, `jackson-json-pointer`, `jackson-manual-references`}. | No serializer |
| `--useJastAddNames` | Set to `true` to use accessor names for relations matching JastAdd naming convention.                | `false`       |

## Supported relations

```
// Directed relations
A.b    -> B;
A.b?   -> B;
A.bs*  -> B;
B      <- A.b  ;
B      <- A.b? ;
B      <- A.bs*;

// Bidirectional relations
A.b   <-> B.a;
A.b   <-> B.a?;
A.b   <-> B.as*;
A.b?  <-> B.a;
A.b?  <-> B.a?;
A.b?  <-> B.as*;
A.bs* <-> B.a;
A.bs* <-> B.a?;
A.bs* <-> B.as*;
```
